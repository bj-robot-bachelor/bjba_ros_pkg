cmake_minimum_required(VERSION 2.8.3)
project(bjba)

set(OpenCV_DIR /usr/share/OpenCV)
set(OpenCV_INCLUDE_DIR /usr/include/opencv2)
set(OpenCV_LIBS /usr/lib/x86_64-linux-gnu/libopencv_core.so)
#set(octomap_DIR /opt/ros/melodic/share/octomap)
## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  geometry_msgs
  message_generation
  OpenCV
  cv_bridge
  image_transport
  tf
  pcl_conversions
  moveit_ros_planning_interface
  easy_handeye
  )

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

################################################
## Declare ROS messages, services and actions ##
################################################

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  Pose.msg 
)

## Generate services in the 'srv' folder
add_service_files(
  FILES
  PointCloudDump.srv
  CPEGetPose.srv
)

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
  sensor_msgs
  geometry_msgs
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES bjba
#  CATKIN_DEPENDS caros_universalrobot roscpp rospy std_msgs
#  DEPENDS system_lib
  CATKIN_DEPENDS message_runtime
)

###########
## Build ##
###########
## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
# include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)


add_executable(cpe_service src/CPE/cpe_service.cpp src/CPE/cpe.cpp src/CPE/cpe.h src/CPE/yaml_loader.h)
add_dependencies(cpe_service bjba_generate_messages_cpp ${catkin_EXPORTED_TARGETS})
target_link_libraries(cpe_service ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} yaml-cpp)

add_executable(cameraPoseEstimator src/CPE/cpe_node.cpp src/CPE/cpe.cpp src/CPE/cpe.h src/CPE/yaml_loader.h)
add_dependencies(cameraPoseEstimator bjba_generate_messages_cpp ${catkin_EXPORTED_TARGETS})
target_link_libraries(cameraPoseEstimator ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} yaml-cpp)


add_executable(sscan src/SphericalScan.cpp)
add_dependencies(sscan bjba_generate_messages_cpp ${catkin_EXPORTED_TARGETS})
target_link_libraries(sscan ${catkin_LIBRARIES})

add_executable(logger_service src/logger_service.cpp src/dlogger/dlogger.cpp src/dlogger/dlogger.h)
add_dependencies(logger_service bjba_generate_messages_cpp ${catkin_EXPORTED_TARGETS})
target_link_libraries(logger_service ${catkin_LIBRARIES})


add_executable(collPub src/collPub.cpp)
target_link_libraries(collPub ${catkin_LIBRARIES})

