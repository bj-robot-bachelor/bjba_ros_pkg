import rospy
import geometry_msgs.msg
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import String
import math
import numpy as np
import tf


def stats(x):
    n = 0
    S = 0.0
    m = 0.0
    for x_i in x:
        n = n + 1
        m_prev = m
        m = m + (x_i - m) / n
        S = S + (x_i - m) * (x_i - m_prev)
    return {'mean': m, 'variance': S/n, 'std': math.sqrt(S/n)}


def stats2(x):
    n = len(x)
    m = sum(x)/n
    v = 0
    for x_i in x:
        v += (x_i-m)*(x_i-m)
    v /= n

    return {'mean': m, 'variance': v, 'std': math.sqrt(v)}


sampleSize = 500
x = [0 for i in range(sampleSize)]
y = [0 for i in range(sampleSize)]
z = [0 for i in range(sampleSize)]
euler_x = [0 for i in range(sampleSize)]
euler_y = [0 for i in range(sampleSize)]
euler_z = [0 for i in range(sampleSize)]

idx = 0
statN = ['mean', 'variance', 'std']



def callback(msg):
    global idx, x, y, z, w, sampleSize

    quat = np.asarray([msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w])
    euler_xyz=tf.transformations.euler_from_quaternion(quat, axes='sxyz')
   
    x[idx] = msg.pose.position.x
    y[idx] = msg.pose.position.y
    z[idx] = msg.pose.position.z
    euler_x[idx] = euler_xyz[0]
    euler_y[idx] = euler_xyz[1]
    euler_z[idx] = euler_xyz[2]

   # print(euler_xyz)

    idx += 1
    idx = idx % (sampleSize)
    print(idx, idx == 0)
    if idx == 0:
        print("calc")
        print('std: x,y,z')
        print str(stats(x)['std']) + ", " + str(stats(y)['std']) + ", " + str(stats(z)['std']) + ", " + str(stats(euler_x)['std'])+ ", " + str(stats(euler_y)['std'])+ ", " + str(stats(euler_z)['std'])


rospy.init_node('cpe_stat', anonymous=True)
rospy.Subscriber("/bjba/camera_pose",  PoseStamped, callback)

rospy.spin()
