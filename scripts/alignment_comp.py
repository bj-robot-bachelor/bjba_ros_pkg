#!/usr/bin/python3.6
import math
import yaml
import argparse
import tf
import numpy as np
import os
import csv

parser = argparse.ArgumentParser()
parser.add_argument("testfolder", nargs="?", type=str, default='anno.yaml/', help="aligmentfile")

args = parser.parse_args()
dirpath = args.testfolder


def get_centroid(tvecs):
    return np.average(tvecs, 0)


def get_dist_to_centroid(tvecs, centroid=None):
    if centroid is None:
        centroid = get_centroid(tvecs)
    return np.linalg.norm(tvecs-centroid, None, 1)


def get_individual_dist(tvecs):
    indivdual_dist = []
    for i1, v1 in enumerate(tvecs):
        for i2, v2 in enumerate(tvecs):
            if i1 > i2:
                indivdual_dist.append(math.sqrt(pow(v1[0]-v2[0], 2)+pow(v1[1]-v2[1], 2)+pow(v1[2]-v2[2], 2)))
    return np.asarray(indivdual_dist)


def get_individual_wdiff(w):
    indivdual_wdist = []
    for i1, v1 in enumerate(w):
        for i2, v2 in enumerate(w):
            if i1 > i2:
                indivdual_wdist.append(tf.transformations.quaternion_multiply(v1, tf.transformations.quaternion_inverse(v2)))
    return np.array(indivdual_wdist)[:, 3]


def yaml2t(path):
    with open(path, "r") as f:
        alignmentParams = list(yaml.load_all(f))[0]

    obj_names = [n for n in alignmentParams if n is not '']
    obj_names.sort()
    tvecs = np.zeros((len(obj_names), 3))
    quats = np.zeros((len(obj_names), 3))
    T = np.zeros((len(obj_names), 4, 4))
    for i, n in enumerate(obj_names):
        T[i, 0, :] = np.array(alignmentParams[n][0])[0:4]
        T[i, 1, :] = np.array(alignmentParams[n][0])[4:8]
        T[i, 2, :] = np.array(alignmentParams[n][0])[8:12]
        T[i, 3, :] = [0, 0, 0, 1]
    tvecs = T[:, 0:3, 3]

    filename=os.path.splitext(os.path.basename(path))[0]
    obj_names = [os.path.splitext(os.path.basename(n))[0] for n in obj_names]
    quats = np.asarray([tf.transformations.quaternion_from_matrix(t) for t in T])
    centroid_dist = get_dist_to_centroid(tvecs)
    ideal_centroid_dist = get_dist_to_centroid(tvecs, np.asarray([0.2567226168769983, 0.1841212026798266, 0.01450989739745197]))
    indivdual_dist = get_individual_dist(tvecs)
    indivdual_wdiff = get_individual_wdiff(quats) 
    data=np.concatenate((centroid_dist,ideal_centroid_dist,indivdual_dist,indivdual_wdiff))
    return (filename,obj_names, tvecs, quats, data)

testfiles = [os.path.join(dirpath, f) for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f))]
testfiles.sort(key=str,reverse=False)
testfiles.reverse()
print(testfiles)

(filename,obj_names, tvecs, quats, data)=yaml2t(testfiles[0])
csv_data=[]
for i,files in enumerate(testfiles):
    
   (filename,obj_names, tvecs, quats, data)=yaml2t(files)
   csv_data.append([filename]+data.tolist())


#print(csv_data)
with open('csvtest.csv', 'wb') as csvfile:
    wr = csv.writer(csvfile)
    for row in csv_data:
      wr.writerow(row)


# (obj_names, tvecs, quats, data) = yaml2t(args.aligmentfile)
# print(obj_names)
# print(centroid_dist)
# print(ideal_centroid_dist)
# print(indivdual_dist)
# print(indivdual_wdiff)


# data.tofile('foo.csv',sep=',',format='%10.5f')

# # tdist = [[math.sqrt(pow(v1[0, 3]-v2[0, 3], 2)+pow(v1[1, 3]-v2[1, 3], 2)+pow(v1[2, 3]-v2[2, 3], 2)) for v1 in T if not np.array_equal(v1, v2)]for v2 in T]
# quats = np.asarray([tf.transformations.quaternion_from_matrix(t) for t in T])
# # print(quats)
# wdist = np.asarray([[q1[3]-q2[3] for q1 in quats if not np.array_equal(q1, q2)]for q2 in quats])

# print(obj_names)
# print("distance from each other")
# for td in tdist:
#     print(td)

# for td in tdist:
#     print('dist mean: '+str(sum(td)/len(td)))

# for wd in wdist:
#     print(wd)

# for wd in wdist:
#     print('w mean: '+str(sum(wd)/len(wd)))
