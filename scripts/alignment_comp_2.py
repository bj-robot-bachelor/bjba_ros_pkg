#!/usr/bin/python3.6
from quaternion_average import averageQuaternions
import math
import yaml
import argparse
import tf
import numpy as np
import os
import csv

parser = argparse.ArgumentParser()
parser.add_argument("testfolder", nargs="?", type=str, default='anno.yaml/', help="aligmentfile")

def get_centroid(tvecs):
    return np.average(tvecs, 0)


def get_dist_to_centroid(tvecs, centroid=None):
    if centroid is None:
        centroid = get_centroid(tvecs)
    return np.linalg.norm(tvecs-centroid, None, 1)


def get_individual_dist(tvecs):
    individual_dist = []
    for i1, v1 in enumerate(tvecs):
        for i2, v2 in enumerate(tvecs):
            if i1 > i2:
                individual_dist.append(math.sqrt(pow(v1[0]-v2[0], 2)+pow(v1[1]-v2[1], 2)+pow(v1[2]-v2[2], 2)))
    return np.asarray(individual_dist)


def get_individual_wdiff(w):
    indivdual_wdist = []
    for i1, v1 in enumerate(w):
        for i2, v2 in enumerate(w):
            if i1 > i2:
                indivdual_wdist.append(tf.transformations.quaternion_multiply(v1, tf.transformations.quaternion_inverse(v2)))
    return np.array(indivdual_wdist)[:, 3]


def yaml2t(path):
    with open(path, "r") as f:
        alignmentParams = list(yaml.load_all(f))[0]

    obj_names = [n for n in alignmentParams if n is not ',']
    obj_names.sort()
    
    tvecs = np.zeros((len(obj_names), 3))
    quats = np.zeros((len(obj_names), 3))
    T = np.zeros((len(obj_names), 4, 4))

    for i, n in enumerate(obj_names):
        T[i, 0, :] = np.array(alignmentParams[n][0])[0:4]
        T[i, 1, :] = np.array(alignmentParams[n][0])[4:8]
        T[i, 2, :] = np.array(alignmentParams[n][0])[8:12]
        T[i, 3, :] = [0, 0, 0, 1]
    tvecs = T[:, 0:3, 3]
    tvecs*=1000
    filename=os.path.splitext(os.path.basename(path))[0]
    obj_names = [os.path.splitext(os.path.basename(n))[0] for n in obj_names]
    quats = np.asarray([tf.transformations.quaternion_from_matrix(t) for t in T])
    
    individual_dist = get_individual_dist(tvecs)
    indivdual_wdiff = get_individual_wdiff(quats) 
    # data=np.concatenate((centroid_dist,ideal_centroid_dist,individual_dist,indivdual_wdiff))
    return (filename,obj_names, tvecs, quats)



args = parser.parse_args()
dirpath = args.testfolder

testfiles = [os.path.join(dirpath, f) for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f))]
testfiles.sort(key=str,reverse=False)
testfiles.reverse()

aligmentKeys=['man_align','noise_poses']
aligmentNumbers=[5,5]

align_groups={}
for key in aligmentKeys:
    align_groups[key]=[]

for names in testfiles:
    for key in aligmentKeys:
        if names.find(key) != -1:
            align_groups[key].append(names)

#print(align_groups)

csv_data=[]
dist2c=np.zeros((5,4))
dist2ic=np.zeros((5,4))
individual_dist=np.zeros((5,6))
quats5=np.zeros((5,4,4))
quat_avrs=np.zeros((4,4))
quat_avrs_individual_ang=np.zeros(6)
labels=['Alignment name','x-axis','q1','q2','q3','q4','q1','q2','q3','q4','q1q2','q1q3','q1q4','q2q3','q2q4','q3q4','q1q2','q1q3','q1q4','q2q3','q2q4','q3q4']
for group in align_groups:
    csv_data.append(labels)
    for i,filepath in enumerate(align_groups[group]):
        (filename,align_names, tvecs, quats)=yaml2t(filepath)
        dist2c[i%5,:] = get_dist_to_centroid(tvecs)
        #dist2ic[i%5,:] = get_dist_to_centroid(tvecs, np.asarray([256.7226168769983, 184.1212026798266, 14.50989739745197]))
        dist2ic[i%5,:] = get_dist_to_centroid(tvecs, np.asarray([256.7592482003158, 184.0831635159178, 15.00752983849367]))
        
        individual_dist[i%5,:]=get_individual_dist(tvecs)
        quats5[i%5,:]=quats
        if i%5 == 4:
            for q in range(quats5.shape[1]):
                quat_avrs[q]=averageQuaternions(quats5[:,q])
            idx=0

            indivdual_wdiff = get_individual_wdiff(quat_avrs) 
            for index,w in enumerate(indivdual_wdiff):  
                print(indivdual_wdiff[index])
                quat_avrs_individual_ang[index]=2*math.acos(abs(indivdual_wdiff[index]))


            # for i1, v1 in enumerate(indivdual_wdiff):
            #     for i2, v2 in enumerate(indivdual_wdiff):
            #         if i1 > i2:
                        
            #             quat_avrs_individual_ang[idx]=2*math.acos(math.pow(np.dot(v1,v2),2)-1)
            #             #quat_avrs_individual_ang[idx]=2*math.acos(math.pow(np.dot(v1,v2),2)-1)
            #             idx+=1
            # print(quat_avrs_individual_ang)

            
            print([filename[:filename.find(group)+len(group)]]+np.average(dist2c,0).tolist()+np.average(dist2c,0).tolist())
            csv_data.append([filename[:filename.find(group)+len(group)]]+[" "]+np.average(dist2c,0).tolist()+np.average(dist2ic,0).tolist()+np.average(individual_dist,0).tolist()+quat_avrs_individual_ang.tolist())


#print(csv_data)
with open('csvtest.csv', 'wb') as csvfile:
    wr = csv.writer(csvfile)
    for row in csv_data:
      wr.writerow(row)


# (align_names, tvecs, quats, data) = yaml2t(args.aligmentfile)
# print(align_names)
# print(centroid_dist)
# print(ideal_centroid_dist)
# print(individual_dist)
# print(indivdual_wdiff)


# data.tofile('foo.csv',sep=',',format='%10.5f')

# # tdist = [[math.sqrt(pow(v1[0, 3]-v2[0, 3], 2)+pow(v1[1, 3]-v2[1, 3], 2)+pow(v1[2, 3]-v2[2, 3], 2)) for v1 in T if not np.array_equal(v1, v2)]for v2 in T]
# quats = np.asarray([tf.transformations.quaternion_from_matrix(t) for t in T])
# # print(quats)
# wdist = np.asarray([[q1[3]-q2[3] for q1 in quats if not np.array_equal(q1, q2)]for q2 in quats])

# print(align_names)
# print("distance from each other")
# for td in tdist:
#     print(td)

# for td in tdist:
#     print('dist mean: '+str(sum(td)/len(td)))

# for wd in wdist:
#     print(wd)

# for wd in wdist:
#     print('w mean: '+str(sum(wd)/len(wd)))
