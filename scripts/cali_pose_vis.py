#!/usr/bin/env python
import numpy as np
import vtk
import argparse
import math


parser = argparse.ArgumentParser()

parser.add_argument("csvfile", nargs="?", type=str, default='pose_comp_data.csv', help="pose_comp_data csv file")
parser.add_argument("cameraPoses", nargs="?", type=str, default='cameraPoses.csv', help="robotPoses csv file")
parser.add_argument("robotPoses", nargs="?", type=str, default='robotPoses.csv', help="cameraPoses csv file")

parser.add_argument("-l", "--logposes", default=False, action='store_true', help="logging poses")
parser.add_argument("-a", "--align", default=False, action='store_true', help="alignsframes based on first frame")


args = parser.parse_args()


def main():
    colors = vtk.vtkNamedColors()
    
    #Camera Pose renderer ---------------------------
    # a renderer and render window
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.SetWindowName("Axes")
    renderWindow.AddRenderer(renderer)
    axes = vtk.vtkAxesActor()
    axes.SetTotalLength(0.1,0.1,0.1)
    axes.AxisLabelsOff()
    

    renderer.AddActor(axes)

    # an interactor
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    interactorStyle=vtk.vtkInteractorStyleTrackballCamera()
    renderWindowInteractor.SetInteractorStyle(interactorStyle)

    # add the actors to the scene
    renderer.SetBackground(colors.GetColor3d("SlateGray"))
    
    
    #Pose difference plotter renderer ---------------------------
    TC=[]
    TR=[]
    
    with open(args.cameraPoses,'r') as f:
        transformAlign=vtk.vtkTransform()
        for index,line in enumerate(f):  
            transformation=vtk.vtkMatrix4x4()

            rotation=map(float,line.split(',')[0:9])
            translation=map(float,line.split(',')[9:12])

            for i in range(3):
                transformation.SetElement(i,3,translation[i])

            for i in range(3):
                for j in range(3):
                    transformation.SetElement(i,j,rotation[i*3+j])
                  

            transformation.SetElement(3,3,1.0)
            
            transform=vtk.vtkTransform()
            transform.Identity()
            if index is 0:
                print(transform)
            transform.SetMatrix(transformation)
            if index is 0:
                print(transform   if i is 0 or len(poses)/2:
        joint_val=move_group.get_current_joint_values()
        joint_val[5]=0
        move_group.set_joint_value_target(joint_val)
        plan = move_group.go(wait=True)
        move_group.stop())
               if i is 0 or len(poses)/2:
        joint_val=move_group.get_current_joint_values()
        joint_val[5]=0
        move_group.set_joint_value_target(joint_val)
        plan = move_group.go(wait=True)
        move_group.stop()
            if args.logposes:   if i is 0 or len(poses)/2:
        joint_val=move_group.get_current_joint_values()
        joint_val[5]=0
        move_group.set_joint_value_target(joint_val)
        plan = move_group.go(wait=True)
        move_group.stop()
                TC.append(trans   if i is 0 or len(poses)/2:
        joint_val=move_group.get_current_joint_values()
        joint_val[5]=0
        move_group.set_joint_value_target(joint_val)
        plan = move_group.go(wait=True)
        move_group.stop()form)

            if index is 0 and args.align:
                transformAlign=transform.GetInverse()
            if args.align:
                transform.SetInput(transformAlign)
            axes = vtk.vtkAxesActor()
            axes.SetTotalLength(0.02,0.02,0.02)
            axes.AxisLabelsOff()
            axes.SetUserTransform(transform)    
            renderer.AddActor(axes)

    with open(args.robotPoses,'r') as f:
        transformAlign=vtk.vtkTransform()
        for i,line in enumerate(f):
            trans=map(float,line.split(',')[0:3])
            rot=map(float,line.split(',')[3:6])

            #rotn=math.fmod(vtk.vtkMath.Norm(rot),2*math.pi)
            rotn=vtk.vtkMath.Norm(rot)

            transform = vtk.vtkTransform()
            transform.Identity()
            transform.Translate(trans[0],trans[1],trans[2])
            
            if args.logposes:
                TR.append(transform)

            #transform.RotateWXYZ(math.sqrt(rot[0]*rot[0]+rot[1]*rot[1]+rot[2]*rot[2]),rot)
            transform.RotateWXYZ(rotn*57.29577951,rot)

            if i is 0 and args.align:
                transformAlign=transform.GetInverse()
            if args.align:
                transform.SetInput(transformAlign)
            axes = vtk.vtkAxesActor()
            axes.SetTotalLength(0.02,0.02,0.02)
            axes.AxisLabelsOff()
            axes.SetUserTransform(transform)    
            renderer.AddActor(axes)

    f = open(args.csvfile, "w")
    if args.logposes:
        for i in range(len(TC)-1):
            transformC = vtk.vtkTransform()
            transformR = vtk.vtkTransform()

            transformC.Identity()
            transformR.Identity()

            transformC.Concatenate(TC[i].GetInverse())
            transformC.Concatenate(TC[i+1])
            transformR.Concatenate(TR[i].GetInverse())
            transformR.Concatenate(TR[i+1])

            posC=transformC.GetPosition()
            posR=transformR.GetPosition()


            rotC=transformC.GetOrientationWXYZ()
            rotR=transformR.GetOrientationWXYZ()

            difTrans=math.sqrt(math.pow(posC[0]-posR[0],2)+math.pow(posC[1]-posR[1],2)+math.pow(posC[2]-posR[2],2))
            difRotAxis=math.sqrt(math.pow(rotC[0+1]-rotR[0+1],2)+math.pow(rotC[1+1]-rotR[1+1],2)+math.pow(rotC[2+1]-rotR[2+1],2))
            difRotAng=math.sqrt(math.pow(rotC[0]-rotR[0],2))

            print(str(difTrans)+", "+str(difRotAxis)+", "+str(difRotAng)+"\n")
            f.write(str(difTrans)+", "+str(difRotAxis)+", "+str(difRotAng)+"\n")

    f.close()
  
    if not args.logposes:
        renderer.GetActiveCamera().Azimuth(50)
        renderer.GetActiveCamera().Elevation(-30)

        renderer.ResetCamera()
        renderWindow.Render()

        # begin mouse interaction
        renderWindowInteractor.Start()


if __name__ == "__main__":
    main()