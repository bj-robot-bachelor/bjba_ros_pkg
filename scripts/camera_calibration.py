import numpy as np
import cv2
from cv2 import aruco
import glob
import yaml
from charuco_dict_integer_from_string import str2arucoDict
import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument("data_folder", nargs="?", type=str, default='cali_images/', help="data folder")
parser.add_argument("charuco_board_yaml", nargs="?", type=str, default='charuco_board.yaml', help="charuco board yaml file location")
parser.add_argument("cali_params_yaml", nargs="?", type=str, default='cali_params.yaml', help="calibration yaml file location")

parser.add_argument("-m", "--manual", default=False, action='store_true', help="lets you see the detected markers for each images. Press any key to continue.")
parser.add_argument("-v", "--verbose", default=False, action='store_true', help="shows arguments parsed, Camera Matrix and Distance Coefficient")


args = parser.parse_args()

if args.verbose:
    print('Arguments are:')
    print(args)
# image folder

with open(args.charuco_board_yaml, "r") as f:
   charuco_board_params = list(yaml.load_all(f))[0]

# ChAruco board variables
DICTIONARY = str2arucoDict(charuco_board_params['DICTIONARY'])



# The calibration part is a modified version of https://github.com/kyle-elsalhi/opencv-examples/blob/master/CalibrationByCharucoBoard/CalibrateCamera.py
# Create constants to be passed into OpenCV and Aruco methods
CHARUCO_BOARD = aruco.CharucoBoard_create(
    squaresX=charuco_board_params['SQUARE_X'],
    squaresY=charuco_board_params['SQUARE_Y'],
    squareLength=charuco_board_params['SQUARE_LENGTH'],
    markerLength=charuco_board_params['MARKER_LENGTH'],
    dictionary=DICTIONARY)

# Create the arrays and variables we'll use to store info like corners and IDs from images processed
corners_all = []  # Corners discovered in all images processed
ids_all = []  # Aruco ids corresponding to corners discovered
image_size = None  # Determined at runtime



    # This requires a set of images or a video taken with the camera you want to calibrate
    # All images used should be the same size, which if taken with the same camera shouldn't be a problem
images = glob.glob(args.data_folder + '*.png')
images.sort()


# Loop through images glob'ed
il=len(images)-1
for ii,iname in enumerate(images):
    if args.verbose:
        print(iname+"  ("+str(ii)+"/"+str(il)+")")

    # Open the image
    img = cv2.imread(iname)
    # imgscale the image
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find aruco markers in the query image
    corners, ids, _ = aruco.detectMarkers(
        image=gray,
        dictionary=DICTIONARY)

    # Outline the aruco markers found in our query image
    img = aruco.drawDetectedMarkers(
        image=img,
        corners=corners)

    # Get charuco corners and ids from detected aruco markers
    response, charuco_corners, charuco_ids = aruco.interpolateCornersCharuco(
        markerCorners=corners,
        markerIds=ids,
        image=gray,
        board=CHARUCO_BOARD)

    # If a Charuco board was found, let's collect image/corner points
    # Requiring at least 20 squares
    if response > 20:
        # Add these corners and ids to our calibration arrays
        corners_all.append(charuco_corners)
        ids_all.append(charuco_ids)

        # Draw the Charuco board we've detected to show our calibrator the board was properly detected
        # img = aruco.drawDetectedCornersCharuco(
        #     image=img,
        #     charucoCorners=charuco_corners,
        #     charucoIds=charuco_ids)

        # If our image size is unknown, set it now
        if not image_size:
            image_size = gray.shape[::-1]

        # Reproportion the image, maxing width or height at 1000
        proportion = max(img.shape) / 1000.0
        img = cv2.resize(img, (int(img.shape[1] / proportion), int(img.shape[0] / proportion)))

        # Pause to display each image, waiting for key pressarguments
        if args.manual:
            cv2.imshow('Charuco board', img)
            cv2.waitKey(0)

    else:
        print("Not able to detect a charuco board in image: {}".format(iname))


    
# Destroy any open CV windows
cv2.destroyAllWindows()

# Make sure at least one image was found
if len(images) < 1:
    # Calibration failed because there were no images, warn the user
    print(
        "Calibration was unsuccessful. No images of charucoboards were found. Add images of charucoboards and use or alter the naming conventions used in this file.")
    # Exit for failure
    exit()

# Make sure we were able to calibrate on at least one charucoboard by checking
# if we ever determined the image size
if not image_size:
    # Calibration failed because we didn't see any charucoboards of the PatternSize used
    print(
        "Calibration was unsuccessful. We couldn't detect charucoboards in any of the images supplied. Try changing the patternSize passed into Charucoboard_create(), or try different pictures of charucoboards.")
    # Exit for failure
    exit()

# Now that we've seen all of our images, perform the camera calibration
# based on the set of points we've discovered
calibration, cameraMatrix, distCoeffs, rvecs, tvecs = aruco.calibrateCameraCharuco(
    charucoCorners=corners_all,
    charucoIds=ids_all,
    board=CHARUCO_BOARD,
    imageSize=image_size,
    cameraMatrix=None,
    distCoeffs=None)

# Print matrix and distortion coefficient to the console
if args.verbose:
    print('Camera Matrix: ')
    print(cameraMatrix)
    print('Distance Coefficients: ')
    print(distCoeffs)

# Save values to be used where matrix+dist is required, for instance for posture estimation
# I save files in a pickle file, but you can use yaml or whatever works for you

data = {"camera_matrix": cameraMatrix.tolist(), "dist_coeff": distCoeffs.tolist()}

with open(args.cali_params_yaml, "w") as f:
    yaml.dump(data, f)

# Print to console our success
print('Calibration successful. Calibration saved to: {}'.format(args.cali_params_yaml))
