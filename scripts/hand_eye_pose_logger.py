#!/usr/bin/env python  
import roslib
import rospy
import math
import tf
from tf.transformations import *
import geometry_msgs.msg
import turtlesim.srv
import tty
import sys
import termios
import math
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import String

transc=[0 for i in range(3)]
quat=[0 for i in range(4)]

def callback(sPose):
    transc[0]=sPose.pose.position.x
    transc[1]=sPose.pose.position.y
    transc[2]=sPose.pose.position.z
    quat[0]=sPose.pose.orientation.x
    quat[1]=sPose.pose.orientation.y
    quat[2]=sPose.pose.orientation.z
    quat[3]=sPose.pose.orientation.w
   
 
    
def q2aa(q):
    s = math.sqrt(1-q[3]*q[3])
    ang=2 * math.acos(q[3])
    if s<0.001:
        x = q[0]
        y = q[1]
        z = q[2]
    else:
        x = ang*q[0] / s
        y = ang*q[1] / s
        z = ang*q[2] / s
    return [x,y,z]

    float angle_rad = math.acos(q0) * 2
    float angle_deg = angle_rad * 180 / math.pi
    float x = q1 / math.sin(angle_rad/2)
    float y = q2 / math.sin(angle_rad/2)
    float z = q3 / math.sin(angle_rad/2)


if __name__ == '__main__':
    rospy.init_node('tf_listener')
    rospy.Subscriber("/bjba/camera_pose", PoseStamped, callback)

    listener = tf.TransformListener()

    orig_settings = termios.tcgetattr(sys.stdin)

    tty.setraw(sys.stdin)
    x = 0
    fc = open("cameraPoses.csv", "w")
    fr = open("robotPoses.csv", "w")
    while x != chr(27): # ESC
        x=sys.stdin.read(1)[0]
        if x is chr(13):
                (transr,qr) = listener.lookupTransform('/base', '/tool0_controller', rospy.Time(0)) 
                aa=q2aa(qr)
                #print(aa)
                rotmatc=quaternion_matrix(quat)[:3,:3].tolist()

                param_str_c=rotmatc[0]+rotmatc[1]+rotmatc[2]+transc
                param_str_r=transr+aa
                print(",".join(map(str, param_str_c))+'\n')
                print(",".join(map(str, param_str_r))+'\n')
                fc.write(",".join(map(str, param_str_c))+'\n')
                fr.write(",".join(map(str, param_str_r))+'\n')


    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)    

