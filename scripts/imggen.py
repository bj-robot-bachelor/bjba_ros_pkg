#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
bridge = CvBridge()

im = np.zeros([100,100])



def callback(data):
    global im
    cv_image = bridge.imgmsg_to_cv2(data, desired_encoding="bgr8")
    cv2.imshow("banan", cv_image)
    cv2.imshow("paere", im)
    key = cv2.waitKey(100);
    if key == 99:
        print("Take screenshot")
        global count
        im = cv_image
        cv2.imwrite("cali_images/cali_image_{:03d}.png".format(count), cv_image)
        count += 1


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('imgen', anonymous=True)

    rospy.Subscriber("camera/color/image_rect_color", Image, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    global count
    count = 1
    listener()