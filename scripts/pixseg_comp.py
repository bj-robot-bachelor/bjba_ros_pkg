import cv2  # import OpenCV
import numpy as np
from blend_modes import soft_light, normal
import glob
import ntpath

images = glob.glob("/home/jens/.bjba/dataset/eval_35_2/*.png")
masks = glob.glob("/home/jens/.bjba/dataset/eval_35_2/pixel_segmentation/*.png")
# images = glob.glob("/home/jens/.bjba/dataset/acc_d415_d30_a30504_custom/*.png")
# masks = glob.glob("/home/jens/.bjba/dataset/acc_d415_d30_a30504_custom/pixel_segmentation/*.png")

images.sort()
masks.sort()

print(images)


for i,img in enumerate(images):
    # Import background image
    background_img_float = cv2.imread(img).astype(float)
    print(background_img_float.shape)
    # Import foreground image
    foreground_img_float = cv2.imread(masks[i]).astype(float)
    #print(foreground_img_float.shape)

    b_channel, g_channel, r_channel = cv2.split(background_img_float)
    alpha_channel = np.ones(b_channel.shape, dtype=b_channel.dtype) * 1 #creating a dummy alpha channel image.
    background_img_float = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))

    b_channel, g_channel, r_channel = cv2.split(foreground_img_float)
    alpha_channel = np.ones(b_channel.shape, dtype=b_channel.dtype) * 1 #creating a dummy alpha channel image.
    foreground_img_float = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))
    # Blend images
    opacity = 0.5  # The opacity of the foreground that is blended onto the background is 70 %.
    blended_img_float = soft_light(background_img_float, foreground_img_float, opacity)

    # Display blended image
    blended_img_uint8 = blended_img_float.astype(np.uint8)  # Convert image to OpenCV native display format

    cv2.imshow('window', blended_img_uint8)
    b_channel, g_channel, r_channel, a = cv2.split(blended_img_uint8)
    a=np.ones(b_channel.shape)*255
    aa=a.astype(np.uint8)
    outimg=cv2.merge((b_channel,g_channel,r_channel,aa))
    s=str("./pixsegcomp/"+ntpath.basename(img))
    print(s)
    cv2.imwrite(s,outimg)
    cv2.waitKey(1)  # Press a key to close window with the image./home/jens/catkin_ws/src/bjba/scripts/
