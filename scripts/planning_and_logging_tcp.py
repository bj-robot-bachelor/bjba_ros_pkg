#!/usr/bin/env python
import sys
import copy

import rospy
import tf
from bjba.srv import PointCloudDump
from bjba.srv import CPEGetPose
from sensor_msgs.msg import Image
from std_msgs.msg import String

import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from geometry_msgs.msg import Pose
from moveit_commander.conversions import pose_to_list

from math import pi
import argparse
import csv
import time
# Parameters---------------------------------------------------------
# moveit parameters
velocityScale = 0.1  # velocity scale from 0 to 1
accelerationScale = 0.1  # acceleration scale from 0 to 1
planningAttempts = 40
# --------------------------------------------------------

# Setup initalize ros node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('robotPlanningSim', anonymous=True)

# Setup logging service and pose estimation service
rospy.wait_for_service("/bjba/point_cloud_dump")
logger = rospy.ServiceProxy("/bjba/point_cloud_dump", PointCloudDump)

# Setup transform listener
listener = tf.TransformListener()
rate = rospy.Rate(10.0)

# seting up moveit move_group wrapper
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
rospy.sleep(2)
move_group.set_num_planning_attempts(planningAttempts)
move_group.set_max_velocity_scaling_factor(velocityScale)
move_group.set_max_acceleration_scaling_factor(accelerationScale)


filename = 'greatJointConfigs/joint_values_r35_a30504_p100.csv'

newBoardPose = True

with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    print("Reading "+filename)
    row_cnt = 1

    for row in csv_reader:
        # Read csv row and convert it to joint values
        joint_taget = map(float, row)
        row_cnt += 1
        print("Joint values "+str(row_cnt)+": "+str(row))
        # Set joint value taget and plan+execute, stop() to ensure nothing is moving
        move_group.set_joint_value_target(joint_taget)
        plan = move_group.go(wait=True)
        move_group.stop()
        # Listen for robot tool tranform and make pose
        time.sleep(.3)
        
        try:
            (trans,rot) = listener.lookupTransform('/base', '/tool0_controller', rospy.Time(0))
            pose=Pose()
            pose.position.x=trans[0]
            pose.position.y=trans[1]
            pose.position.z=trans[2]
            pose.orientation.x=rot[0]
            pose.orientation.y=rot[1]
            pose.orientation.z=rot[2]
            pose.orientation.w=rot[3]

            logger(pose)
        except rospy.ServiceException, e:
            print(e)


        

