import numpy as np
import vtk
import argparse
import math
from pose_generator import *

colors = vtk.vtkNamedColors()
    
#Camera Pose renderer ---------------------------
# a renderer and render window
renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.SetWindowName("Axes")
renderWindow.AddRenderer(renderer)
axes = vtk.vtkAxesActor()
axes.SetTotalLength(0.1,0.1,0.1)
axes.AxisLabelsOff()

renderer.AddActor(axes)

# an interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

interactorStyle=vtk.vtkInteractorStyleTrackballCamera()
renderWindowInteractor.SetInteractorStyle(interactorStyle)

# add the actors to the scene
renderer.SetBackground(colors.GetColor3d("SlateGray"))


poses=generate_vtk_poses(0.70, 0, 0, 30, 80, 6, 0.3, 110)

for pose in poses:
    transform = vtk.vtkTransform()
    transform.PostMultiply()
    transform.Identity()
    transform.RotateX(pose[3])
    transform.RotateY(pose[4])
    transform.RotateZ(pose[5])
    transform.Translate(pose[0],pose[1],pose[2])

    axes = vtk.vtkAxesActor()
    axes.SetTotalLength(0.02,0.02,0.02)
    axes.AxisLabelsOff()
    axes.SetUserTransform(transform)    
    renderer.AddActor(axes)


renderWindow.Render()
renderWindowInteractor.Start()

