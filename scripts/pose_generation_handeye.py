import numpy as np
import cv2
from cv2 import aruco
import glob
import yaml
from charuco_dict_integer_from_string import str2arucoDict
import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument("data_folder", nargs="?", type=str, default='cali_images/', help="data folder")
parser.add_argument("charuco_board_yaml", nargs="?", type=str, default='charuco_board.yaml', help="charuco board yaml file location")
parser.add_argument("cali_params_yaml", nargs="?", type=str, default='cali_params.yaml', help="calibration yaml file location")
parser.add_argument("-v", "--verbose", default=False, action='store_true', help="Prints information")

args = parser.parse_args()
if args.verbose:
    print('Arguments are:')
    print(args)  

def q2aa(q):
    s = math.sqrt(1-q[0]*q[0])
    ang=2 * math.acos(q[0])
    if s<0.001:
        x = q[1]
        y = q[2]
        z = q[3]
    else:
        x = ang*q[1] / s
        y = ang*q[2] / s
        z = ang*q[3] / s
    return [x,y,z]

with open(args.charuco_board_yaml, "r") as f:
    charuco_board_params = list(yaml.load_all(f))[0]

# ChAruco board variables
DICTIONARY = str2arucoDict(charuco_board_params['DICTIONARY'])

# Create constants to be passed into OpenCV and Aruco methods
CHARUCO_BOARD = aruco.CharucoBoard_create(
    squaresX=charuco_board_params['SQUARE_X'],
    squaresY=charuco_board_params['SQUARE_Y'],
    squareLength=charuco_board_params['SQUARE_LENGTH'],
    markerLength=charuco_board_params['MARKER_LENGTH'],
    dictionary=DICTIONARY)

# Create the arrays and variables we'll use to store info like corners and IDs from images processed
corners_all = []  # Corners discovered in all images processed
ids_all = []  # Aruco ids corresponding to corners discovered
image_size = None  # Determined at runtime



    # This requires a set of images or a video taken with the camera you want to calibrate
    # All images used should be the same size, which if taken with the same camera shouldn't be a problem
images = glob.glob(args.data_folder + '*.png')
images.sort()

# Pose list creation for hand eye calibration

fc = open("cameraPoses.csv", "w")
fr = open("robotPoses.csv", "w")

with open(args.cali_params_yaml, "r") as f:
    camera_params = list(yaml.load_all(f))[0]

cameraMatrix=np.array(camera_params['camera_matrix'])
distCoeffs=np.array(camera_params['dist_coeff'])
il=len(images)-1
for ii,iname in enumerate(images):

    # Open the image
    img = cv2.imread(iname)
    # Grayscale the image
    detectedCorners, detectedIds, rejectedCorners=aruco.detectMarkers(
        image=img,
        dictionary=DICTIONARY)

    detectedCorners, detectedIds, rejectedCorners, recoveredIdxs = aruco.refineDetectedMarkers(
        image=img,
        board=CHARUCO_BOARD,
        detectedCorners=detectedCorners,
        detectedIds=detectedIds,
        rejectedCorners=rejectedCorners,
        cameraMatrix=cameraMatrix,
        distCoeffs=distCoeffs)

    
    retval, charucoCorners, charucoIds = aruco.interpolateCornersCharuco(
        markerCorners=detectedCorners,
        markerIds=detectedIds,
        image=img,
        board=CHARUCO_BOARD,
        cameraMatrix=cameraMatrix,
        distCoeffs=distCoeffs)


    retval, rvec, tvec = aruco.estimatePoseCharucoBoard(
        charucoCorners=charucoCorners, 
        charucoIds=charucoIds, 
        board=CHARUCO_BOARD, 
        cameraMatrix=cameraMatrix, 
        distCoeffs=distCoeffs)

    if (retval):
        rotc = np.empty((3,3))
        cv2.Rodrigues(rvec, rotc)

        transc = -np.dot(rotc.transpose() , tvec)

        param_str_c=rotc.transpose().flatten().tolist()+transc.flatten().tolist()
        fc.write(",".join(map(str, param_str_c))+'\n')     
    

    pname=iname.replace('.png','.pcd')

    if args.verbose:
        print(iname+"  ("+str(ii)+"/"+str(il)+")\n"+pname)

    with open(pname,'r') as f:
        for i,line in enumerate(f):  
            if i>9:
                break
            for word in line.split():
                if word == 'VIEWPOINT':
                    transr=map(float,line.split()[1:4])
                    rotr=q2aa(map(float,line.split()[4:8]))    
    
    param_str_r=transr+rotr
    
    if args.verbose:
        print("CameraPose:"+str(transc.flatten())+"\nRobotPose: "+str(transr))
    fr.write(",".join(map(str, param_str_r))+'\n')

print("Poses placed in cameraPose.csv and robotPoses.csv")