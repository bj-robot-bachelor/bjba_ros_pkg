#!/usr/bin/env python
import math
import geometry_msgs.msg
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler
PI = math.pi

def get_vtk_pose(t, x, y, z, phi, r):
    theta = 2 * PI * t + PI
    x = r * math.cos(2 * PI * t) * math.cos(phi) + x
    y = r * math.sin(2 * PI * t) * math.cos(phi) + y
    z = r * math.sin(phi) + z

    pose = [0 for i in range(6)]
    pose[0] = x
    pose[1] = y
    pose[2] = z
    pose[3] = math.degrees(0)
    pose[4] = math.degrees(phi)
    pose[5] = math.degrees(theta)

    return pose


def get_pose(t, x, y, z, phi, r):
    theta = 2 * PI * t + PI
    x = r * math.cos(2 * PI * t) * math.cos(phi) + x
    y = r * math.sin(2 * PI * t) * math.cos(phi) + y
    z = r * math.sin(phi) + z
    quat = quaternion_from_euler(PI+PI/4, phi, theta)
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = x
    pose_goal.position.y = y
    pose_goal.position.z = z
    pose_goal.orientation.x = quat[0]
    pose_goal.orientation.y = quat[1]
    pose_goal.orientation.z = quat[2]
    pose_goal.orientation.w = quat[3]

    return pose_goal


def generate_poses(x, y, z, angLower, angUpper, angSteps, radius, npts):
    poseN = npts/2

    angInc = (angUpper-angLower)/(angSteps-1)
    angles = [math.radians(angLower+angInc*i) for i in range(angSteps)]

    print("angles" + str(angles))


    rotlen = [math.cos(ang)*radius*PI for ang in angles]
    rotlenSum = sum(rotlen)

    rotPoses = [int(math.ceil(rotlen[i]/rotlenSum*poseN)) for i in range(len(rotlen))]
    print("angle poses"+str(rotPoses))
    rott = [[0.5*(0.5/float(rotPoses[j]))+(0.5/float(rotPoses[j]))*i for i in range(rotPoses[j])] for j in range(len(rotPoses))]
    
    posesL = []
    posesR = []

    for i in range(len(rott)):
        if i % 2 is 0:
            for j in range(len(rott[i])):
                posesL.append(get_pose(rott[i][j], x, y, z, angles[i], radius))
                posesR.append(get_pose(1-rott[i][j], x, y, z, angles[i], radius))
        else:
            for j in range(len(rott[i])):
                posesL.append(get_pose(0.5-rott[i][j], x, y, z, angles[i], radius))
                posesR.append(get_pose(0.5+rott[i][j], x, y, z, angles[i], radius))

    print(str(len(posesL*2))+" poses generated")

    return posesL+posesR


def generate_vtk_poses(x, y, z, angLower, angUpper, angSteps, radius, npts):
    poseN = npts/2

    angInc = (angUpper-angLower)/(angSteps-1)
    angles = [math.radians(angLower+angInc*i) for i in range(angSteps)]
    print(angInc)
    print(angles)
    print("angles" + str(angles))

    rotlen = [math.cos(ang)*radius*PI for ang in angles]
    rotlenSum = sum(rotlen)

    rotPoses = [int(math.ceil(rotlen[i]/rotlenSum*poseN)) for i in range(len(rotlen))]
    print("angle poses"+str(rotPoses))
    rott = [[0.5*(0.5/float(rotPoses[j]))+(0.5/float(rotPoses[j]))*i for i in range(rotPoses[j])] for j in range(len(rotPoses))]
    
    posesL = []
    posesR = []


    for i in range(len(rott)):
        if i % 2 is 0:
            for j in range(len(rott[i])):
                posesL.append(get_vtk_pose(rott[i][j], x, y, z, angles[i], radius))
                posesR.append(get_vtk_pose(1-rott[i][j], x, y, z, angles[i], radius))
        else:
            for j in range(len(rott[i])):
                posesL.append(get_vtk_pose(0.5-rott[i][j], x, y, z, angles[i], radius))
                posesR.append(get_vtk_pose(0.5+rott[i][j], x, y, z, angles[i], radius))

    print(str(len(posesL*2))+" poses generated")

    return posesL+posesR
