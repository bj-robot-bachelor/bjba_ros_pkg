#!/usr/bin/env python  

import roslib
import rospy
import math
import tf
from tf.transformations import *
from bjba.srv import *
from std_msgs.msg import String
from ur_msgs.msg import IOStates
import geometry_msgs.msg
import yaml
import moveit_commander
import moveit_msgs.msg
from moveit_commander.conversions import pose_to_list
from geometry_msgs.msg import *
import sys



if __name__ == '__main__':
    with open('/home/bjba/catkin_ws/src/bjba/scripts/WayPoints.yaml','r') as f:
        yaml_p = list(yaml.load_all(f))
    WayPoint=yaml_p[0]['WayPoint']

    rospy.init_node('robotLogginManager')

    listener = tf.TransformListener()

    log=rospy.ServiceProxy('/bjba/point_cloud_dump',PointCloudDump)

    moveit_commander.roscpp_initialize(sys.argv)
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    rospy.sleep(1)

    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    move_group.set_max_acceleration_scaling_factor(0.5)
    move_group.set_max_velocity_scaling_factor(0.5)
    # We can get the joint values from the group and adjust some of the values:
    for points in WayPoint:
        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        print("joint_q: "+str(points))
        move_group.go(points,wait=True)
        move_group.stop()
        rospy.sleep(0.3)
        (t,q) = listener.lookupTransform('/base', '/tool0_controller', rospy.Time(0)) 
        P=Pose(position=Point(*t),orientation=Quaternion(*q))
        log(P)
        print("logging succesfull")
        

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
