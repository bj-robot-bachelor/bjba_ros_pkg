#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from geometry_msgs.msg import PoseStamped
from math import pi

from actionlib_msgs.msg import GoalStatusArray
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from pose_generator import generate_poses
from time import gmtime, strftime, localtime

# Parameters---------------------------------------------------------
# pose generation parameters
rotation_center = [0.7, 0.0, 0.0]  # center of rotation in meters in MoveIt world coordinates
lowerAngle = 30.0  # degrees
higherAngle = 50.0   # degrees
angleLevels = 4  # number angles between higer and lower limits minimum 2
offset=0.02
distance2center = 0.30+offset  # distance to the rotation center
numberOfPoses = 100  # number of poses this values might not be the number of generated poses since its will have to be an even number and there is some round up going on
# moveit parameters
planningAttempts = 50  # number of planning attempt to find the shortest path


# utility
reset_joint_to_0 = True
# ---------------------------------------------------------


planSucces = True
def planing_status_callback(msg):
    global planSucces
    if len(msg.status_list) > 1:
        planSucces = (msg.status_list[0].status <= 3)
    if not planSucces:
        print("planning failed")



#initialize rosnode
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('robotPlanningSim', anonymous=True)
#initializing node subscribers
#rospy.Subscriber("/move_group/status", GoalStatusArray, planing_status_callback)


robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
rospy.sleep(2)

move_group.set_num_planning_attempts(planningAttempts)

if reset_joint_to_0:
    move_group.set_joint_value_target([0, 0, 0, 0, 0, 0])
    plan = move_group.go(wait=True)
    move_group.stop()

poses = generate_poses(rotation_center[0],
                       rotation_center[1],
                       rotation_center[2],
                       lowerAngle, higherAngle,
                       angleLevels,
                       distance2center,
                       numberOfPoses)

#joint_value_file = open("joint_values_"+strftime("%m_%d_%H_%M", localtime())+".csv", "w")
joint_value_file = open("joint_values_r"+str(int(100*(distance2center-offset)))+"_a"+str(int(lowerAngle*1000+higherAngle*10+angleLevels))+"_p"+str(numberOfPoses)+".csv", "w")
for i, pose in enumerate(poses):
    # Set wrist 3 to 0 before starting and before switching rotation sides
    if i is 0 or i is len(poses)/2:
        joint_val = move_group.get_current_joint_values()
        joint_val[5] = 0
        move_group.set_joint_value_target(joint_val)
        plan = move_group.go(wait=True)
        move_group.stop()
    move_group.set_pose_target(pose)
    plan = move_group.go(wait=True)
    move_group.stop()
    
    joint_values = move_group.get_current_joint_values()
    string_joint_values = str(', '.join([str(value) for value in joint_values])+"\n")
    print(string_joint_values)
    joint_value_file.write(string_joint_values)
