#!/usr/bin/python3.6
import math
import yaml
import argparse
import tf
import numpy as np
import os



def get_centroid(tvecs):
    return np.average(tvecs, 0)


def get_dist_to_centroid(tvecs, centroid=None):
    if centroid is None:
        centroid = get_centroid(tvecs)
    return np.linalg.norm(tvecs-centroid, None, 1)


def get_individual_dist(tvecs):
    indivdual_dist = []
    for i1, v1 in enumerate(tvecs):
        for i2, v2 in enumerate(tvecs):
            if i1 > i2:
                indivdual_dist.append(math.sqrt(pow(v1[0]-v2[0], 2)+pow(v1[1]-v2[1], 2)+pow(v1[2]-v2[2], 2)))
    return np.asarray(indivdual_dist)


def get_individual_wdiff(w):
    indivdual_wdist = []
    for i1, v1 in enumerate(w):
        for i2, v2 in enumerate(w):
            if i1 > i2:
                indivdual_wdist.append(tf.transformations.quaternion_multiply(v1, tf.transformations.quaternion_inverse(v2)))
    return np.array(indivdual_wdist)[:, 3]


def yaml2t(path):
    with open(path, "r") as f:
        alignmentParams = list(yaml.load_all(f))[0]

    names = [n for n in alignmentParams if n is not '']
    names.sort()
    tvecs = np.zeros((len(names), 3))
    quats = np.zeros((len(names), 3))
    T = np.zeros((len(names), 4, 4))
    for i, n in enumerate(names):
        T[i, 0, :] = np.array(alignmentParams[n][0])[0:4]
        T[i, 1, :] = np.array(alignmentParams[n][0])[4:8]
        T[i, 2, :] = np.array(alignmentParams[n][0])[8:12]
        T[i, 3, :] = [0, 0, 0, 1]
    tvecs = T[:, 0:3, 3]
    names = [os.path.splitext(os.path.basename(n))[0] for n in names]
    quats = np.asarray([tf.transformations.quaternion_from_matrix(t) for t in T])
    centroid_dist = get_dist_to_centroid(tvecs)
    ideal_centroid_dist = get_dist_to_centroid(tvecs, np.asarray([0.2567226168769983, 0.1841212026798266, 0.01450989739745197]))
    indivdual_dist = get_individual_dist(tvecs)
    indivdual_wdiff = get_individual_wdiff(quats) 
    data=np.concatenate((centroid_dist,ideal_centroid_dist,indivdual_dist,indivdual_wdiff))
    return (names, tvecs, quats, data)


