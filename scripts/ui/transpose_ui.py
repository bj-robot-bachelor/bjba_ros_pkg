# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'transpose.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(194, 155)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.input_x = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_x.setObjectName("input_x")
        self.gridLayout_2.addWidget(self.input_x, 0, 1, 1, 1)
        self.input_rx = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_rx.setObjectName("input_rx")
        self.gridLayout_2.addWidget(self.input_rx, 0, 3, 1, 1)
        self.input_y = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_y.setObjectName("input_y")
        self.gridLayout_2.addWidget(self.input_y, 1, 1, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout_2.addWidget(self.label_8, 0, 2, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout_2.addWidget(self.label_7, 2, 2, 1, 1)
        self.input_rz = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_rz.setObjectName("input_rz")
        self.gridLayout_2.addWidget(self.input_rz, 2, 3, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)
        self.input_z = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_z.setObjectName("input_z")
        self.gridLayout_2.addWidget(self.input_z, 2, 1, 1, 1)
        self.input_ry = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.input_ry.setObjectName("input_ry")
        self.gridLayout_2.addWidget(self.input_ry, 1, 3, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 2, 0, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.gridLayout_2.addWidget(self.label_9, 1, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.btn_send_t = QtWidgets.QPushButton(self.centralwidget)
        self.btn_send_t.setObjectName("btn_send_t")
        self.gridLayout.addWidget(self.btn_send_t, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_8.setText(_translate("MainWindow", "Rx"))
        self.label_7.setText(_translate("MainWindow", "Rz"))
        self.label_3.setText(_translate("MainWindow", "x"))
        self.label.setText(_translate("MainWindow", "z"))
        self.label_9.setText(_translate("MainWindow", "Ry"))
        self.label_2.setText(_translate("MainWindow", "y"))
        self.btn_send_t.setText(_translate("MainWindow", "Send"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

