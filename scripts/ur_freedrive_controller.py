#!/usr/bin/env python
import rospy
from std_msgs.msg import String

import socket
import time

HOST = "10.42.0.144"
PORT = 30002

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
time.sleep(0.05)
s.send ("set_digital_out(2,True)" + "\n")
time.sleep(0.1)
s.send ("set_digital_out(7,True)" + "\n")
time.sleep(2)



def callback(data):
    #print("movel(p[{}, {}, {}, {}, {}, {}])".format(data.x, data.y, data.z, data.rx, data.ry, data.rz))
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
    #s.send("movel(p[{}, {}, {}, {}, {}, {}])".format(data.x, data.y, data.z, data.rx, data.ry, data.rz) + "\n")
    if data.data == "false":
        s.send("end_freedrive_mode()" + "\n")
        print("freedrive stopped")
    elif data.data == "true":
        #s.send("freedrive_mode()" + "\n")
        #s.send("sleep(20)" + "\n")
        s.send("def myProg():\n\tfreedrive_mode()\nsleep({})\nend".format(10))
        print("freedrive started")
    else:
        print("Fack");
    #time.sleep(2)
    #s.send("movel(p[-0.5, 0.2, 0.25, 2.103, 2.223, 0.124])" + "\n")


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('ur_freedrive_controller', anonymous=True)

    rospy.Subscriber("enable", String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()