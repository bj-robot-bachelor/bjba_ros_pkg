#!/usr/bin/env python
import rospy
import numpy as np

from geometry_msgs.msg import Pose

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import random



#plt.ion()

fig = plt.figure()
ax = Axes3D(fig)

sequence_containing_x_vals = list(range(0, 100))
sequence_containing_y_vals = list(range(0, 100))
sequence_containing_z_vals = list(range(0, 100))

random.shuffle(sequence_containing_x_vals)
random.shuffle(sequence_containing_y_vals)
random.shuffle(sequence_containing_z_vals)

sc = ax.scatter(sequence_containing_x_vals, sequence_containing_y_vals, sequence_containing_z_vals)
plt.show()

def callback(data, args):
    global sc
    global fig

    sequence_x_vals = list(range(0, 100))
    sequence_y_vals = list(range(0, 100))
    sequence_z_vals = list(range(0, 100))

    random.shuffle(sequence_x_vals)
    random.shuffle(sequence_y_vals)
    random.shuffle(sequence_z_vals)

    #sc._offsets3d = (sequence_x_vals, sequence_y_vals, sequence_z_vals)

    sc._set_ydata(sequence_y_vals)
    fig.canvas.draw()
    print(Pose)





def listener():


    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.

    rospy.init_node('imgen', anonymous=True)

    rospy.Subscriber("charuco_board_pose", Pose, callback, (2))

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()




if __name__ == '__main__':

    listener()