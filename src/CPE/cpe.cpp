#include "cpe.h"


/// Takes in the path for the camera calibration and charuco board yaml files.
CPE::CPE(string cali_params_path, string charuco_path) {
    ros::NodeHandle n("bjba");
    pub = n.advertise<geometry_msgs::PoseStamped>("camera_pose", 1000);
    cp = caliParams(cali_params_path);
    cb = charucoBoard(charuco_path);

    //Initializing objects used in rgb_callback for board detection
    charucoboard = aruco::CharucoBoard::create(cb.square_x, cb.square_y, cb.square_l, cb.marker_l,
                                               aruco::getPredefinedDictionary(cb.dictionary));
    board = charucoboard.staticCast<aruco::Board>();
    detectorParams = aruco::DetectorParameters::create();

    //ROS_INFO("sending back response: [%ld]", (long int)res.sum);
    ostringstream CM;
    CM << cp.camera_matrix;
    ostringstream DC;
    DC << cp.distance_coeff;

    ROS_INFO("\nCameraMatrix\n  %s\n\nDistance coefficients\n %s\n\nCharuco Board:\n %i %i %f %f %s", CM.str().c_str(),
             DC.str().c_str(), cb.square_x, cb.square_y, cb.square_l, cb.marker_l, cb.dictionary_string.c_str());
}
/// Receives and rgb image for camera pose estimation and publishes valid poses.
/** It is using the ros params charuco_path and cali_params_path for the detectiong and localization look in the launchfiles for more info.. */

void CPE::rgb_callback(const sensor_msgs::ImageConstPtr &msg) {
    {
//ROS_INFO("I heard: image");
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception &e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        cv::Mat image = cv_ptr->image;

        Mat imageCopy;

        vector<int> markerIds, charucoIds;
        vector<vector<Point2f> > markerCorners, rejectedMarkers;
        vector<Point2f> charucoCorners;
        Vec3d rvec, tvec;


        aruco::detectMarkers(image, aruco::getPredefinedDictionary(cb.dictionary), markerCorners, markerIds,
                             detectorParams, rejectedMarkers);


        aruco::refineDetectedMarkers(image, board, markerCorners, markerIds, rejectedMarkers, cp.camera_matrix,
                                     cp.distance_coeff);

// interpolate charuco corners
        int interpolatedCorners = 0;
        if (markerIds.size() > 0)
            interpolatedCorners =
                    aruco::interpolateCornersCharuco(markerCorners, markerIds, image, charucoboard,
                                                     charucoCorners, charucoIds, cp.camera_matrix, cp.distance_coeff);


        Point2f prevCC(-1, -1);
        int prevCI = -1;



        bool validPose = false;
        if (!charucoIds.empty())
            validPose = aruco::estimatePoseCharucoBoard(charucoCorners, charucoIds, charucoboard, cp.camera_matrix,
                                                        cp.distance_coeff, rvec, tvec);
// draw results
        image.copyTo(imageCopy);
        if (markerIds.size() > 0) {
            aruco::drawDetectedMarkers(imageCopy, markerCorners);
        }

        /*        if (false && rejectedMarkers.size() > 0)
                   aruco::drawDetectedMarkers(imageCopy, rejectedMarkers, noArray(), Scalar(100, 0, 2);

               if (interpolatedCorners > 0) {
                   Scalar color;
                   color = Scalar(255, 0, 0);
                   aruco::drawDetectedCornersCharuco(imageCopy, charucoCorners, charucoIds, color);
               }*/


        if (validPose) {
            aruco::drawAxis(imageCopy, cp.camera_matrix, cp.distance_coeff, rvec, tvec, 0.2);

            Mat rot = Mat(3, 3, CV_64FC1);
            Rodrigues(rvec, rot);

            Mat pose = -rot.t() * Mat(tvec);

            tf::Vector3 rotvec(rvec[0], rvec[1], rvec[2]);
            tf::Quaternion qb(0, 0, 0, 1);
            tf::Quaternion qcb;
            qcb.setRotation(rotvec, rotvec.length());

            tf::Quaternion q = qb * qcb.inverse();


            geometry_msgs::PoseStamped pmsg;

            pmsg.header.frame_id = "map";

            pmsg.pose.position.x = pose.at<double>(0);
            pmsg.pose.position.y = pose.at<double>(1);
            pmsg.pose.position.z = pose.at<double>(2);


            pmsg.pose.orientation.x = q.getX();
            pmsg.pose.orientation.y = q.getY();
            pmsg.pose.orientation.z = q.getZ();
            pmsg.pose.orientation.w = q.getW();

            pub.publish(pmsg);


            static tf::TransformBroadcaster br;
            tf::Transform transform;
            transform.setOrigin(tf::Vector3(pose.at<double>(0), pose.at<double>(1), pose.at<double>(2)));
            transform.setRotation(q);
            br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "charuco_board", "camera_pose"));
        }

        cv::imshow("camera feed", imageCopy);
        waitKey(1);

    }
}

bool CPE::cpe_service(bjba::CPEGetPose::Request &req, bjba::CPEGetPose::Response &res) {
//ROS_INFO("I heard: image");
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(req.image, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return false;
    }

    cv::Mat image = cv_ptr->image;

    vector<int> markerIds, charucoIds;
    vector<vector<Point2f> > markerCorners, rejectedMarkers;
    vector<Point2f> charucoCorners;
    Vec3d rvec, tvec;


    aruco::detectMarkers(image, aruco::getPredefinedDictionary(cb.dictionary), markerCorners, markerIds,
                         detectorParams, rejectedMarkers);


    aruco::refineDetectedMarkers(image, board, markerCorners, markerIds, rejectedMarkers, cp.camera_matrix,
                                 cp.distance_coeff);

// interpolate charuco corners
    int interpolatedCorners = 0;
    if (markerIds.size() > 0)
        interpolatedCorners =
                aruco::interpolateCornersCharuco(markerCorners, markerIds, image, charucoboard,
                                                 charucoCorners, charucoIds, cp.camera_matrix, cp.distance_coeff);


    bool validPose = false;
    if (!charucoIds.empty())
        validPose = aruco::estimatePoseCharucoBoard(charucoCorners, charucoIds, charucoboard, cp.camera_matrix,
                                                    cp.distance_coeff, rvec, tvec);


    if (validPose) {
        Mat rot = Mat(3, 3, CV_64FC1);
        Rodrigues(rvec, rot);

        Mat pose = -rot.t() * Mat(tvec);

        tf::Vector3 rotvec(rvec[0], rvec[1], rvec[2]);
        tf::Quaternion qb(0, 0, 0, 1);
        tf::Quaternion qcb;
        qcb.setRotation(rotvec, rotvec.length());

        tf::Quaternion q = qb * qcb.inverse();


        geometry_msgs::PoseStamped pmsg;

        pmsg.header.frame_id = "map";

        pmsg.pose.position.x = pose.at<double>(0);
        pmsg.pose.position.y = pose.at<double>(1);
        pmsg.pose.position.z = pose.at<double>(2);

        pmsg.pose.orientation.x = q.getX();
        pmsg.pose.orientation.y = q.getY();
        pmsg.pose.orientation.z = q.getZ();
        pmsg.pose.orientation.w = q.getW();

        pub.publish(pmsg);

        res.pose = pmsg.pose;

        static tf::TransformBroadcaster br;
        tf::Transform transform;
        transform.setOrigin(tf::Vector3(pose.at<double>(0), pose.at<double>(1), pose.at<double>(2)));
        transform.setRotation(q);
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "charuco_board", "camera_pose"));
    }

    return validPose;
}

