#ifndef PROJECT_CAMERA_POSE_ESTIMATOR_H
#define PROJECT_CAMERA_POSE_ESTIMATOR_H

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/PoseStamped.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <opencv2/calib3d.hpp>

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>

#include "yaml_loader.h"

#include <vector>
#include <fstream>
#include <ostream>
#include <string>
#include "math.h"
#include "bjba/CPEGetPose.h"

using namespace std;
using namespace cv;
using namespace yl;

class CPE
        {
public:
    CPE(string cali_params_path ,string charuco_path);
    void rgb_callback(const sensor_msgs::ImageConstPtr &msg);
    bool cpe_service(bjba::CPEGetPose::Request &req, bjba::CPEGetPose::Response &res);
private:
    charuco_board cb;
    cali_params cp;
    ros::Publisher pub;

    Ptr<aruco::CharucoBoard> charucoboard;
    Ptr<aruco::Board> board;
    Ptr<aruco::DetectorParameters> detectorParams;


};

#endif //PROJECT_CAMERA_POSE_ESTIMATOR_H
