#include "cpe.h"

int main(int argc, char **argv){
    //ROS setup
    ros::init(argc, argv, "listener");

    ros::NodeHandle n("bjba");

    string charuco_path, cali_params_path, camera_color_topic;
    n.param<std::string>("charuco_path", charuco_path, "charuco_board_17x14_0.05.yaml");
    n.param<std::string>("cali_params_path", cali_params_path, "cali_params.yaml");
    n.param<std::string>("camera_color_topic", camera_color_topic, "/camera/color/image_rect_color");

    CPE cpe(cali_params_path,charuco_path);

    ros::Subscriber sub = n.subscribe(camera_color_topic, 1, &CPE::rgb_callback, &cpe);

    ros::spin();

    return 0;
}
