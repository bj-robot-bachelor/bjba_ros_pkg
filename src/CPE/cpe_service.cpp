
#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/Image.h>
#include "cpe.h"
#include "bjba/CPEGetPose.h"


using namespace std;


int main(int argc, char **argv) {

    ros::init(argc, argv, "CPE_Service");

    ros::NodeHandle nh("bjba");

    string charuco_path, cali_params_path, camera_color_topic;
    nh.param<std::string>("charuco_path", charuco_path, "charuco_board_17x14_0.05.yaml");
    nh.param<std::string>("cali_params_path", cali_params_path, "cali_params.yaml");

    CPE cpe(cali_params_path,charuco_path);

    ros::ServiceServer service = nh.advertiseService("CPEGetPose", &CPE::cpe_service,&cpe);

    ros::spin();

    return 0;
}
