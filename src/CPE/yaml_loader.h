#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <string>
#include <fstream>
#include <iostream>
#include "yaml-cpp/yaml.h"

#ifndef PROJECT_YAML_LOADER_H
#define PROJECT_YAML_LOADER_H
using namespace std;
using namespace cv;

/*!
 This namespace adds functionality regarding loading parameters from yaml file specifically  camera calbration parameters and charuco board parameters used in open cv
*/
namespace yl {
    /// camera calibration parameter structure.
    struct cali_params {
        Mat camera_matrix;
        Mat distance_coeff;
    };
    /// charuco board structure.
    struct charuco_board {
        int square_x;
        int square_y;
        float square_l;
        float marker_l;
        int dictionary;
        string dictionary_string;
    };

    /// converts string with the name of an charuco dict to the coresponding dictionary integer used by opencv.
    static int string2arucoDictInt(string dict) {
        if (dict == "DICT_4X4_50")return cv::aruco::DICT_4X4_50;
        if (dict == "DICT_4X4_100")return cv::aruco::DICT_4X4_100;
        if (dict == "DICT_4X4_250")return cv::aruco::DICT_4X4_250;
        if (dict == "DICT_4X4_1000")return cv::aruco::DICT_4X4_1000;

        if (dict == "DICT_5X5_50")return cv::aruco::DICT_5X5_50;
        if (dict == "DICT_5X5_100")return cv::aruco::DICT_5X5_100;
        if (dict == "DICT_5X5_250")return cv::aruco::DICT_5X5_250;
        if (dict == "DICT_5X5_1000")return cv::aruco::DICT_5X5_1000;

        if (dict == "DICT_6X6_50")return cv::aruco::DICT_6X6_50;
        if (dict == "DICT_6X6_100")return cv::aruco::DICT_6X6_100;
        if (dict == "DICT_6X6_250")return cv::aruco::DICT_6X6_250;
        if (dict == "DICT_6X6_1000")return cv::aruco::DICT_6X6_1000;

        if (dict == "DICT_7X7_50")return cv::aruco::DICT_7X7_50;
        if (dict == "DICT_7X7_100")return cv::aruco::DICT_7X7_100;
        if (dict == "DICT_7X7_250")return cv::aruco::DICT_7X7_250;
        if (dict == "DICT_7X7_1000")return cv::aruco::DICT_7X7_1000;
    }

    ///  returns a calibration parameter object given a path to a calibration parameter yaml file.
    static cali_params caliParams(string path) {
        ifstream f(path);
        string cali_file((std::istreambuf_iterator<char>(f)),
                         (std::istreambuf_iterator<char>()));
        YAML::Node node = YAML::Load(cali_file);

        cali_params c;

        c.camera_matrix = Mat_<double>(3, 3);
        if (node["camera_matrix"]) {
            cout << "camera_matrix loaded from: "<<path << endl;
            for (int y = 0; y < c.camera_matrix.rows; y++)
                for (int x = 0; x < c.camera_matrix.cols; x++) {
                    c.camera_matrix.at<double>(y, x) = node["camera_matrix"][y][x].as<double>();
                }
        }

        if (node["dist_coeff"]) {
            cout << "dist_coeff loaded from: "<<path  << endl;
            c.distance_coeff = Mat_<double>(1, node["dist_coeff"][0].size());
            for (int i = 0; i < node["dist_coeff"][0].size(); i++)
                c.distance_coeff.at<double>(0, i) = node["dist_coeff"][0][i].as<double>();

        }
        return c;
    }
    ///  returns a charuco board object given a path to a calibration parameter yaml file.
    static charuco_board charucoBoard(string path) {
        ifstream f(path);
        string charuco_file((std::istreambuf_iterator<char>(f)),
                            (std::istreambuf_iterator<char>()));
        YAML::Node node = YAML::Load(charuco_file);

        charuco_board b;

        if (node["SQUARE_X"])
            b.square_x = node["SQUARE_X"].as<int>();
        if (node["SQUARE_Y"])
            b.square_y = node["SQUARE_Y"].as<int>();
        if (node["SQUARE_LENGTH"])
            b.square_l = node["SQUARE_LENGTH"].as<float>();
        if (node["MARKER_LENGTH"])
            b.marker_l = node["MARKER_LENGTH"].as<float>();
        if (node["DICTIONARY"]) {
            b.dictionary = string2arucoDictInt(node["DICTIONARY"].as<string>());
            b.dictionary_string = node["DICTIONARY"].as<string>();
        }
        cout<<"charuco_board loaded from: "<<path<<endl;
        return b;
    }

}

#endif //PROJECT_YAML_LOADER_H
