#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <bjba/PointCloudDump.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <easy_handeye/TakeSample.h>

#include <vector>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <boost/format.hpp>
#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>
#include <unistd.h>


#define PI 3.14159265

using namespace std;

inline bool exits(const std::string &name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "move_group_interface_tutorial");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    ros::ServiceClient client = node_handle.serviceClient<bjba::PointCloudDump>("/bjba/point_cloud_dump");
    ros::ServiceClient sampleclient = node_handle.serviceClient<easy_handeye::TakeSample>(
            "/easy_handeye_eye_on_hand/take_sample");

    bjba::PointCloudDump srv;
    easy_handeye::TakeSample ssrv;
    //easy_handeye/





    static const std::string PLANNING_GROUP = "manipulator";

    // The :move_group_interface:`MoveGroupInterface` class can be easily
    // setup using just the name of the planning group you would like to control and plan for.
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

    // We will use the :planning_scene_interface:`PlanningSceneInterface`
    // class to add and remove collision objects in our "virtual world" scene
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

    moveit::planning_interface::MoveGroupInterface::Plan my_plan;

    // Raw pointers are frequently used to refer to the planning group for improved performance.
    const robot_state::JointModelGroup *joint_model_group =
            move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

    move_group.setMaxAccelerationScalingFactor(0.3);
    move_group.setMaxVelocityScalingFactor(0.3);

    tf::TransformListener tflistener;


    string answer = "";
    while (!(answer == "y" || answer == "n")) {
        cout << "Do you want to load a list of joint configurations from file ? (y/n)" << endl;
        cin >> answer;
    }

    if (answer == "n") {
        ofstream outfile;
        outfile.open("joint_configs.csv");

        while (!(answer == "y" || answer == "n")) {
            cout << "Do you want to load default path values ? (y/n)" << endl;
            cin >> answer;
        }

        vector<string> pnames = {"Radius", "CenterX", "CenterY", "CenterZ", "NrRotations",
                                 "NrMeasurementFirstRotation", "Angle"};
        double pvals[] = {0.5, 0.6, 0, 0, 4, 32, 60};

        if (answer == "n") {
            for (int i = 0; i < pnames.size(); i++) {
                while (true) {
                    cout << "Type in " << pnames[i] << endl;
                    cin >> answer;

                    try {
                        pvals[i] = stod(answer);
                        break;
                    }
                    catch (std::invalid_argument ex) {
                        cout << ex.what() << endl;
                    }
                }
            }
        }


        double r = pvals[0];
        double center[3] = {pvals[1], pvals[2], pvals[3]};
        double rots = pvals[4];
        double steps = pvals[5]/2;
        double stepD = steps / (rots + 1);
        double phi = pvals[6] * PI / 180;
        double phiI = ((80 - pvals[6]) / rots) * PI / 180;

        auto startPose = move_group.getCurrentJointValues();

        for (int s = 0; s < 2; s++) {
            for (int j = 0; j < rots; j++) {
                bool even = (j % 2 != 0);
                double t;
                for (double i = 0; i < steps; i++) {
                    if (even)
                        t = 0.5 + (steps - i) / steps;
                    else
                        t = 0.5 + i / steps;

                    t/=2.0;
                    if(s=1)
                        t=1-t;

                    double theta = 2 * PI * t + PI;
                    double x = r * cos(2 * PI * t) * cos(phi) + center[0];
                    double y = r * sin(2 * PI * t) * cos(phi) + center[1];
                    double z = r * sin(phi) + center[2];
                    
                    tf::Quaternion q;

                    q.setRPY(PI + PI / 4, phi, theta);

                    geometry_msgs::Pose target_pose;

                    target_pose.orientation.x = q.getX();
                    target_pose.orientation.y = q.getY();
                    target_pose.orientation.z = q.getZ();
                    target_pose.orientation.w = q.getW();
                    target_pose.position.x = x;
                    target_pose.position.y = y;
                    target_pose.position.z = z;

                    move_group.setPoseTarget(target_pose);

                    bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
                    if (success)
                        move_group.move();

                    move_group.stop();

                    vector<double> jointValues = move_group.getCurrentJointValues();

                    outfile << boost::format("%d,%d,%d,%d,%d,%d\n") % jointValues[0] % jointValues[1] % jointValues[2] %
                               jointValues[3] % jointValues[4] % jointValues[5];

                }
                steps -= stepD;
                phi += phiI;

            }
            outfile.close();
        }
    }
    if (answer == "y") {

        string path;
        while (!exits(path)) {
            cout << "Enter file path:" << endl;
            cin >> path;
        }
        double scale = 0;
        while (true) {
            cout << "Type in acc/vel percentage" << endl;
            cin >> answer;

            try {
                scale = stod(answer);
                break;
            }
            catch (std::invalid_argument ex) {
                cout << ex.what() << endl;
            }
        }
        string hand2eye = "";
        while (!(hand2eye == "y" || hand2eye == "n")) {
            cout << "Hand eye calibration? (y/n)" << endl;
            cin >> hand2eye;

            static tf::TransformBroadcaster br;


            move_group.setMaxAccelerationScalingFactor(scale);
            move_group.setMaxVelocityScalingFactor(scale);

            ifstream infile;
            infile.open(path);
            string l;
            string s2d;
            vector<double> q(6, 0);

            int cnt = 0;
            while (getline(infile, l)) {
                istringstream s(l);
                while (getline(s, s2d, ',')) {
                    q[cnt] = stod(s2d);

                    //cout <<cnt<< "  .  " << s2d << endl;
                    cnt++;
                    if (cnt == 6) {
                        for (int i = 0; i < q.size(); i++)
                            cout << q[i] << ' ';
                        cnt = 0;
                        move_group.setJointValueTarget(q);
                        bool success = (move_group.plan(my_plan) ==
                                        moveit::planning_interface::MoveItErrorCode::SUCCESS);
                        if (success) {

                            cout << endl;
                            move_group.move();
                            move_group.stop();

                            usleep(300000);
                            tf::StampedTransform transform;
                            tf::StampedTransform transform2;


                            if (hand2eye == "y") {
                                if (sampleclient.call(ssrv))
                                    ROS_INFO("Taking sample");
                                else {
                                    ROS_ERROR("Failed to take sample");

                                }
                            } else {
                                try {
                                    tflistener.lookupTransform("/charuco_board", "/camera_pose", ros::Time(0), transform);
                                    //tflistener.lookupTransform("/base", "/tool0_controller", ros::Time(0), transform);

                                }
                                catch (tf::TransformException ex) {
                                    ROS_ERROR("%s", ex.what());
                                    ros::Duration(2.0).sleep();
                                }

                                geometry_msgs::Pose p;
                                p.position.x = transform.getOrigin().getX();
                                p.position.y = transform.getOrigin().getY();
                                p.position.z = transform.getOrigin().getZ();
                                p.orientation.w = transform.getRotation().getW();
                                p.orientation.x = transform.getRotation().getX();
                                p.orientation.y = transform.getRotation().getY();
                                p.orientation.z = transform.getRotation().getZ();


                                srv.request.pose = p;
                                if (client.call(srv))
                                    ROS_INFO("Logging data");
                                else {
                                    ROS_ERROR("Failed to call service point cloud dump");
                                    return 1;
                                }

                            }
                        }
                    }

                }
            }


            infile.close();
        }

    }
    return 0;
}