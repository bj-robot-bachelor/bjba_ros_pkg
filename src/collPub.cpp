#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

// MoveIt!
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/GetStateValidity.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/ApplyPlanningScene.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/robot_state/conversions.h>


int main(int argc, char **argv) {
    ros::init(argc, argv, "collPub");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::NodeHandle node_handle;
    ros::Duration sleep_time(1.0);
    sleep_time.sleep();
    sleep_time.sleep();

    ros::ServiceClient planning_scene_diff_client = node_handle.serviceClient<moveit_msgs::ApplyPlanningScene>(
            "apply_planning_scene");

// Define the attached object message

    moveit_msgs::AttachedCollisionObject attached_object;
    attached_object.link_name = "base";
    /* The header must contain a valid TF frame*/
    attached_object.object.header.frame_id = "base";
    /* The id of the object */
    attached_object.object.id = "plane";


    /* Define a box to be attached */
    shape_msgs::SolidPrimitive table;
    table.type = table.BOX;
    table.dimensions.resize(3);
    table.dimensions[0] = 1.40;
    table.dimensions[1] = 0.80;
    table.dimensions[2] = 0.05;

    /* Define a box to be attached */
    geometry_msgs::Pose pose;
    pose.orientation.w = 1.0;
    pose.position.x=-0.575;
    pose.position.y=0;
    pose.position.z=-0.05/2;

    attached_object.object.primitives.push_back(table);
    attached_object.object.primitive_poses.push_back(pose);


// Add an object into the environment
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ROS_INFO("Adding the object into the world at the location of the base.");
    moveit_msgs::PlanningScene planning_scene;
    planning_scene.world.collision_objects.push_back(attached_object.object);
    planning_scene.is_diff = true;
    planning_scene_diff_client.waitForExistence();
// and send the diffs to the planning scene via a service call:
    moveit_msgs::ApplyPlanningScene srv;
    srv.request.scene = planning_scene;
    planning_scene_diff_client.call(srv);



}