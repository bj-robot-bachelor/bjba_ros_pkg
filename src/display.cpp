
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/PoseStamped.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <opencv2/calib3d.hpp>

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>

#include "camera_pose_estimator/yaml_loader.h"

#include <vector>
#include <fstream>
#include <ostream>
#include <string>
#include "math.h"


using namespace std;
using namespace cv;
using namespace yl;

charuco_board cb;
cali_params cp;
ros::Publisher pub;

Ptr<aruco::CharucoBoard> charucoboard;
Ptr<aruco::Board> board;
Ptr<aruco::DetectorParameters> detectorParams;

/// Receives and rgb image for camera pose estimation and published the pose if its valid.
/// It is using the ros params charuco_path and cali_params_path for the detectiong and localization look in the launchfiles for more info.
void rgb_callback(const sensor_msgs::ImageConstPtr &msg) {
    //ROS_INFO("I heard: image");
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv::Mat image = cv_ptr->image;

    Mat imageCopy;

    vector< int > markerIds, charucoIds;
    vector< vector< Point2f > > markerCorners, rejectedMarkers;
    vector< Point2f > charucoCorners;
    Vec3d rvec, tvec;

    aruco::detectMarkers(image, aruco::getPredefinedDictionary(cb.dictionary), markerCorners, markerIds, detectorParams, rejectedMarkers);

    aruco::refineDetectedMarkers(image, board, markerCorners, markerIds, rejectedMarkers, cp.camera_matrix, cp.distance_coeff);

    // interpolate charuco corners
    int interpolatedCorners = 0;
    if(markerIds.size() > 0)
        interpolatedCorners =
                aruco::interpolateCornersCharuco(markerCorners, markerIds, image, charucoboard,
                                                 charucoCorners, charucoIds, cp.camera_matrix, cp.distance_coeff);

    // estimate charuco board pose
    bool validPose = false;
    if(cp.camera_matrix.total() != 0)
        validPose = aruco::estimatePoseCharucoBoard(charucoCorners, charucoIds, charucoboard, cp.camera_matrix, cp.distance_coeff, rvec, tvec);

    // draw results
    image.copyTo(imageCopy);
    if(markerIds.size() > 0) {
        aruco::drawDetectedMarkers(imageCopy, markerCorners);
    }

    if(false && rejectedMarkers.size() > 0)
        aruco::drawDetectedMarkers(imageCopy, rejectedMarkers, noArray(), Scalar(100, 0, 255));

    if(interpolatedCorners > 0) {
        Scalar color;
        color = Scalar(255, 0, 0);
        aruco::drawDetectedCornersCharuco(imageCopy, charucoCorners, charucoIds, color);
    }

    if(validPose) {
        aruco::drawAxis(imageCopy, cp.camera_matrix, cp.distance_coeff, rvec, tvec, 0.2);

        Mat rot = Mat(3, 3, CV_64FC1);
        Rodrigues(rvec, rot);

        Mat pose = -rot.t() * Mat(tvec);

        tf::Vector3 rotvec(rvec[0], rvec[1], rvec[2]);
        tf::Quaternion qb(0, 0, 0, 1);
        tf::Quaternion qcb;
        qcb.setRotation(rotvec, rotvec.length());

        tf::Quaternion q = qb * qcb.inverse();


        geometry_msgs::PoseStamped pmsg;

        pmsg.header.frame_id = "map";

        pmsg.pose.position.x = pose.at<double>(0);
        pmsg.pose.position.y = pose.at<double>(1);
        pmsg.pose.position.z = pose.at<double>(2);

        //q.inverse();
        pmsg.pose.orientation.x = q.getX();
        pmsg.pose.orientation.y = q.getY();
        pmsg.pose.orientation.z = q.getZ();
        pmsg.pose.orientation.w = q.getW();

        pub.publish(pmsg);


        static tf::TransformBroadcaster br;
        tf::Transform transform;
        transform.setOrigin(tf::Vector3(pose.at<double>(0), pose.at<double>(1), pose.at<double>(2)));
        transform.setRotation(q);
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "camera_pose"));
    }
    cv::imshow("camera feed", imageCopy);
    waitKey(3);

}


int main(int argc, char **argv) {
    //ROS setup
    ros::init(argc, argv, "listener");

    ros::NodeHandle n("bjba");

    ros::Subscriber sub = n.subscribe("/camera/color/image_rect_color", 6, rgb_callback);

    ros::Subscriber sub2 = n.subscribe("/camera/color/image_rect_color", 6, rgb_callback);

    pub = n.advertise<geometry_msgs::PoseStamped>("camera_pose", 1000);

    //Loading parameter yaml files from ROS parameters
    string charuco_path, cali_params_path;
    n.param<std::string>("charuco_path", charuco_path, "charuco_board_17x14_0.05.yaml");
    n.param<std::string>("cali_params_path", cali_params_path, "cali_params.yaml");
    cp=caliParams(cali_params_path);
    cb=charucoBoard(charuco_path);

    ostringstream CM;
    CM<< cp.camera_matrix;
    ostringstream DC;
    DC<< cp.distance_coeff;

    ROS_INFO("\nCameraMatrix\n  %s\n\nDistance coefficients\n %s\n\nCharuco Board:\n %i %i %f %f %s",CM.str().c_str(),DC.str().c_str(),cb.square_x,cb.square_y,cb.square_l,cb.marker_l, cb.dictionary_string.c_str());
    //ROS_INFO("sending back response: [%ld]", (long int)res.sum);

    //Initializing objects used in rgb_callback for board detection
    charucoboard = aruco::CharucoBoard::create(cb.square_x,cb.square_y, cb.square_l,cb.marker_l, aruco::getPredefinedDictionary(cb.dictionary));
    board= charucoboard.staticCast<aruco::Board>();
    detectorParams= aruco::DetectorParameters::create();

    ros::spin();

    return 0;
}
