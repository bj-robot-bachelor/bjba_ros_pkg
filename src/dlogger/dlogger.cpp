 //
// Created by bl on 3/1/19.
//

#include "dlogger.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

//#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <string>
#include <iostream>
#include <ctime>

#include <boost/format.hpp>

Dlogger::Dlogger(vector<string> images_topics, vector<string> pointcloud_topics, double scale) {

    ros::NodeHandle nh;

    unitScale=scale;
    for(string topic : images_topics){
        ROS_INFO("Made Image subsciber at: %s", topic.c_str());
        imageSubs.push_back(nh.subscribe<sensor_msgs::Image>(topic, 1, boost::bind(&Dlogger::imageCallback, this, _1, topic)));
    }

    for(string topic : pointcloud_topics){
        ROS_INFO("Made PointCloud2 subsciber at: %s", topic.c_str());
        pointcloudSubs.push_back(nh.subscribe<sensor_msgs::PointCloud2>(topic, 1, boost::bind(&Dlogger::pointcloudCallback, this, _1, topic)));
    }

    dataset_dir = string(getenv("HOME")) + "/.bjba/dataset";

    dataset_name = get_time();

    system(("mkdir -p " + dataset_dir + "/" + dataset_name).c_str());

}

Dlogger::~Dlogger() {}

void Dlogger::imageCallback(const sensor_msgs::ImageConstPtr &msg, string topic) {
    ROS_DEBUG("Head image on topic: %s", topic.c_str());
    imageMsgs[topic] = msg;
    topicUpdated[topic] = true;
}

void Dlogger::pointcloudCallback(const sensor_msgs::PointCloud2ConstPtr &msg, string topic){
    ROS_DEBUG("Head pointcloud on topic: %s", topic.c_str());
    pointcloudMsgs[topic] = msg;
    topicUpdated[topic] = true;
}

bool Dlogger::log(geometry_msgs::Pose pose) {
    //reset topic update states
    //for(auto s : topicUpdated) s.second = false;

    for(auto s : topicUpdated) topicUpdated[s.first] = false;

    //while(!newData()) {ros::Duration(.1).sleep();};

    //Log data
    string time = get_time();

    counter++;

    char letter = 'a';

    for(const auto imagemsg : imageMsgs) {
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(*imagemsg.second, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception &e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            break;
        }

        std::ostringstream fname;
        fname << boost::format("%s/%s/%s_%04i_%s.png") % dataset_dir % dataset_name % time % counter % letter++;

        cout << "fname: " << fname.str() << endl;
        ROS_DEBUG("saved image at %s", fname.str().c_str());

        cv::Mat image = cv_ptr->image;
        cv::imwrite(fname.str(), image);
    }

    letter = 'a';

    for(const auto &pointcloudmsg : pointcloudMsgs) {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::fromROSMsg(*pointcloudmsg.second, *cloud);

        Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
        transform (0,0) = transform (0,0) * unitScale;
        transform (1,1) = transform (1,1) * unitScale;
        transform (2,2) = transform (2,2) * unitScale;

        if(unitScale != 1.0){
            pcl::transformPointCloud (*cloud, *cloud, transform);
        }

        std::ostringstream fname;
        fname << boost::format("%s/%s/%s_%04i_%s.pcd") % dataset_dir % dataset_name % time % counter % letter++;

        cout << "fname: " << fname.str() << endl;

        cloud->sensor_origin_ = {pose.position.x, pose.position.y, pose.position.z, 0};
        cloud->sensor_orientation_  = {pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z};

        pcl::io::savePCDFileBinary(fname.str(), *cloud);
    }

    return true;
}

string Dlogger::get_time() {
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y%m%d-%H%M%S", timeinfo);

    return string(buffer);
}

bool Dlogger::newData() {
    //Check if all data is new and return false if not
    for(auto s : topicUpdated) {
        cout << s.first << "  ---  " << (s.second?"true\n":"false\n");
        if(!s.second) return false;
    }
    return true;
}


