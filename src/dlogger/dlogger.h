//
// Created by bl on 3/1/19.
//

#ifndef PROJECT_DLOGGER_H
#define PROJECT_DLOGGER_H

#include <ros/ros.h>

#include <geometry_msgs/Pose.h>
#include <vector>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <mutex>

using namespace std;

class Dlogger {
public:
    Dlogger(vector<string> images_topics, vector<string> pointcloud_topics, double scale = 1.0);
    ~Dlogger();

    bool log(geometry_msgs::Pose pose);

private:
    string dataset_dir, dataset_name;

    int counter = 0;

    mutex imageMutex;
    mutex pointcloudMutex;

    map<string, sensor_msgs::ImageConstPtr> imageMsgs;
    map<string, sensor_msgs::PointCloud2ConstPtr> pointcloudMsgs;

    map<string, bool> topicUpdated;

    vector<ros::Subscriber> imageSubs;
    vector<ros::Subscriber> pointcloudSubs;

    double unitScale=1.0;

    bool newData();

    void imageCallback(const sensor_msgs::ImageConstPtr &msg, string topic);
    void pointcloudCallback(const sensor_msgs::PointCloud2ConstPtr &msg, string topic);

    string get_time();
};


#endif //PROJECT_DLOGGER_H
