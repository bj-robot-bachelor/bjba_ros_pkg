#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/Pose.h>
#include <vector>

//pcl specific
#include <sensor_msgs/PointCloud2.h>

#include "bjba/PointCloudDump.h"

#include "dlogger/dlogger.h"

using namespace std;

Dlogger* dlogger;

bool pointCloudDump(bjba::PointCloudDump::Request &req, bjba::PointCloudDump::Response &res)
{
    res.succes = dlogger->log(req.pose);
    return res.succes;
}

int main(int argc, char **argv) {

    ros::init(argc, argv, "logger");

    ros::NodeHandle nh("bjba");

    ros::ServiceServer service = nh.advertiseService("point_cloud_dump", pointCloudDump);

    vector<string> pointcloud_topics;
    vector<string> image_topics;
    double unitScale;
    nh.param<vector<string>>("log/pointclouds", pointcloud_topics, {"/cloud_in"});
    nh.param<vector<string>>("log/images", image_topics, {"/image_in"});
    nh.param("log/point_cloud_Scale",unitScale,1.0);


    dlogger = new Dlogger(image_topics, pointcloud_topics,unitScale);

    ros::spin();

    return 0;
}
