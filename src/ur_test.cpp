#include <vector>
#include <cmath>
#include <iostream>
#include <caros/serial_device_si_proxy.h>
#include "ros/ros.h"


#define PI 3.14159265

using namespace std;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "my_ros_node");
    ros::NodeHandle nh;
    caros::SerialDeviceSIProxy sd_sip(nh, "caros_universalrobot");

    double r = 0.2;
    double center[3]={-0.5,0,0.2};
    double speed=100;
    double blend=0.025;
    double steps=64;
    double phi = 30*PI/180;
    double phi2 = phi + PI/2;

    //for (int j = 0; j < 3; ++j) {

        for (double i = 0; i < steps; i++) {
            double t = 0.15 + 0.5*i/steps;
            double theta = 2 * PI * t - PI/2;
            double x = r * cos(2 * PI * t)*cos(phi) + center[0];
            double y = r * sin(2 * PI * t)*cos(phi) + center[1];
            double z = r * sin(phi) + center[3];

            cout << "t:" << t << " X:" << x << " Y:" << y << endl;
            rw::math::Transform3D<> pose_T(
                    rw::math::Vector3D<>(x, y, z),
                    rw::math::Rotation3D<>(
                            cos(theta), -sin(theta)*cos(phi2), sin(theta)*sin(phi2),
                            sin(theta),  cos(theta)*cos(phi2),-cos(theta)*sin(phi2),
                            0,           sin(phi2),            cos(phi2)
                    ));

            //if(i==0)
            //    sd_sip.movePtpT(pose_T,speed);
            //else
                sd_sip.moveServoT(pose_T, speed);
        }
    //}






/*
    rw::math::Transform3D<> goal_T(
            rw::math::Vector3D<>(-0.5,-0.2,0.5),
            rw::math::Rotation3D<>(
                    -1,0,0,
                    0,1,0,
                    0,0,-1
            ));


    rw::math::Transform3D<> goal_T2(
            rw::math::Vector3D<>(-0.5,0.2,0.5),
            rw::math::Rotation3D<>(
                    -1,0,0,
                    0,1,0,
                    0,0,-1
            ));

    rw::math::Transform3D<> goal_T3(
            rw::math::Vector3D<>(-0.6,0.4,0.5),
            rw::math::Rotation3D<>(
                    -1,0,0,
                    0,1,0,
                    0,0,-1
            ));

    ROS_INFO_STREAM("Moving to '" << goal_T << "'.");
    bool ret = false;
    //ret = sd_sip.movePtp(goal_q);
    ret = sd_sip.movePtpT(goal_T,40,0.1);
    ret = sd_sip.movePtpT(goal_T2,50,0.1);
    ret = sd_sip.movePtpT(goal_T3,60,0.1);

*/
    return 0;
}
